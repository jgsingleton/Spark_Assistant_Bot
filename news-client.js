//==================================================================================
// 'Spark' - Intelligent Software Agent Dissertation Project by Jesse Singleton
//==================================================================================
// news-client.js -- news API refactor and requests
//==================================================================================
// Adapted and refactored from Microsoft (Alyssa Ong ) https://github.com/alyssaong1/NodeNewsBot.
//================================================================================== 

var restify = require('restify');
var prompts = require ('./prompts');
var rp = require('request-promise');
var builder = require('botbuilder');
var BINGNEWSKEY = '05983d582287461393aa35551e637b22';


function getTopNews(session, results, next) {
    if (results.response && results.response.entity !== '(quit)') {
                //Show user that we're processing their request
                session.send(prompts.loading);
                //Build the url
                var numResults = 10;
                var market = "en-US";
                var url = "https://api.cognitive.microsoft.com/bing/v5.0/news/?" 
                    + "category=" + results.response.entity + "&count=" + numResults + "&mkt=" + market + "&originalImg=true";
                //Options for the request
                var options = {
                    uri: url,
                    headers: {
                        'Ocp-Apim-Subscription-Key': BINGNEWSKEY
                    },
                    json: true
                }
                //Make the request
                rp(options).then(function (body){
                    sendTopNews(session, results, body);
                })
                .catch(function (err){
                    console.log(err.message);
                    session.send(prompts.newserror);
                }).finally(function () {
                    session.endDialog();
                });
            } else {
                session.endDialog(prompts.newserror);
            }
        }

function sendTopNews(session, results, body){
    session.send(prompts.msgTopNews, {category:results.response});
    var allArticles = body.value;
    var cards = [];
    for (var i = 0; i < 10; i++){
        var article = allArticles[i];
        // Create a card for the article
        cards.push(new builder.HeroCard(session)
            .title(article.name)
            .subtitle(article.datePublished)
            .images([
                //handle if thumbnail is empty
                builder.CardImage.create(session, article.image.contentUrl)
            ])
            .buttons([
                builder.CardAction.dialogAction(session, "moredetails", article.description, "Short snippet"),
                builder.CardAction.openUrl(session, article.url, "Full article")
            ]));
    }
    var msg = new builder.Message(session)
        .textFormat(builder.TextFormat.xml)
        .attachmentLayout(builder.AttachmentLayout.carousel)
        .attachments(cards);
    session.send(msg);
}

module.exports = {
    getTopNews: getTopNews
};

