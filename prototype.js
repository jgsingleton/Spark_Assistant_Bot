//==================================================================================
// 'Spark' - Intelligent Software Agent Dissertation Project by Jesse Singleton
//==================================================================================
// prototype.js -- main code
//==================================================================================
// ACKNOWLEDGEMENTS
// Original template provided by Microsoft. Available at https://docs.botframework.com/en-us/node/builder/overview/
// Weather Client code adapted from Anthony Chu. Available at http://anthonychu.ca/post/bot-framework-luis-node-luisdialog/
// News Code adapted from Microsoft (Alyssa Ong ) https://github.com/alyssaong1/NodeNewsBot. 
// The entire project is opensource under the MIT license and available at 
//==================================================================================

var restify = require('restify');
var builder = require('botbuilder');
var prompts = require ('./prompts');
var keywords = require ('./keywords');
var rp = require('request-promise');
var weatherClient = require('./wunderground-client');
var newsClient = require ('./news-client');


//=========================================================
// Bot Configuration
//=========================================================

var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url); 
});

server.get(/.*/, restify.serveStatic({
	'directory': '.',
	'default': 'index.html'
}));

// Doesn't need this for local testing'
var connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID || '57b2431d-dcfd-46ae-a28d-33b3d8488973',
    appPassword: process.env.MICROSOFT_APP_PASSWORD || 'EYQ0WBKiXOdh2mDvJDkh8wn'
});

var bot = new builder.UniversalBot(connector);
server.post('/api/messages', connector.listen());

// Local Debugging Console
// var connector = new builder.ConsoleConnector().listen();
// var bot = new builder.UniversalBot(connector)

//Api Keys
var BINGNEWSKEY = '05983d582287461393aa35551e637b22';




// Create LUIS recognizer that points at our model and add it as the root '/' dialog for our Cortana Bot.
var model = 'https://api.projectoxford.ai/luis/v2.0/apps/c413b2ef-382c-45bd-8ff0-f76d60e2a821?subscription-key=ddb12a7c3feb4d2ca9e23dd347341477&q=';
var recognizer = new builder.LuisRecognizer(model);
var dialog = new builder.IntentDialog({ recognizers: [recognizer] });
var intents = new builder.IntentDialog();

//=========================================================
// Main Implementation
//=========================================================

// Regexes prevented wasted calls
intents.matches(keywords.help, '/help');
intents.matches(keywords.news, '/searchnews');
intents.matches(keywords.topnews, '/topnews');
intents.matches(keywords.trending, '/trending');
intents.matches(keywords.menu, '/menu');

bot.dialog('/', dialog);

//Say Hello
dialog.matches(keywords.hello, [
    function (session, args, next) {
        builder.Prompts.text(session, prompts.botHello);
        next();
    }
]);

//List Commands
dialog.matches(keywords.help, [
      function (session, args, next) {
        builder.Prompts.text(session, prompts.botHelp);
        next();
      }

]);

//=========================================================
// Weather Command
//=========================================================

dialog.matches(keywords.weather, [
    function (session, args, next) {
        builder.Prompts.text(session, prompts.locationcheck); 
    },
    (session, results) => {
        weatherClient.getCurrentWeather(results.response, (responseString) => {
            session.send(responseString);
        });
    }
]);


//=========================================================
// News Command 
//=========================================================
bot.beginDialogAction("topnews", "/topnews");
bot.beginDialogAction("searchnews", "/searchnews");
bot.beginDialogAction("trending", "/trending");

dialog.matches(keywords.news, [
    function (session, args, next) {
         builder.Prompts.choice(session, prompts.promptCategory, prompts.choiceCategories);
    },
    (session, results, next) => {
        newsClient.getTopNews(session,results,next, (responseString) => {
            session.send(responseString);
        });
    }
]);

dialog.matches(keywords.menu, [
    function (session, args, next) {
        //send card message menu
        msg = new builder.Message(session)
            .attachments([
                new builder.HeroCard(session)
                    .title("Main Menu")
                    .subtitle("What would you like to do next?")
                    .images([
                        builder.CardImage.create(session, "http://i.imgur.com/I3fYOM2.jpg")
                    ])
                    .buttons([
                        builder.CardAction.dialogAction(session, "topnews", null, "Top News"),
                        builder.CardAction.dialogAction(session, "searchnews", null, "Search News"),
                        builder.CardAction.dialogAction(session, "trending", null, "Get Trending")
                    ])
            ]);
        session.endDialog(msg);
    }
]);


//=========================================================
// Alarm Command
//=========================================================

dialog.matches(keywords.alarm, [
    function (session, args, next) {
        // Resolve and store any entities passed from LUIS.
        var title = builder.EntityRecognizer.findEntity(args.entities, 'builtin.alarm.title');
        var time = builder.EntityRecognizer.resolveTime(args.entities);
        var alarm = session.dialogData.alarm = {
          title: title ? title.entity : null,
          timestamp: time ? time.getTime() : null  
        };
        
        // Prompt for title
        if (!alarm.title) {
            builder.Prompts.text(session, prompts.alarmname);
        } else {
            next();
        }
    },
    function (session, results, next) {
        var alarm = session.dialogData.alarm;
        if (results.response) {
            alarm.title = results.response;
        }

        // Prompt for time (title will be blank if the user said cancel)
        if (alarm.title && !alarm.timestamp) {
            builder.Prompts.time(session, prompts.alarmtime);
        } else {
            next();
        }
    },
    function (session, results) {
        var alarm = session.dialogData.alarm;
        if (results.response) {
            var time = builder.EntityRecognizer.resolveTime([results.response]);
            alarm.timestamp = time ? time.getTime() : null;
        }
        
        // Set the alarm (if title or timestamp is blank the user said cancel)
        if (alarm.title && alarm.timestamp) {
            // Save address of who to notify and write to scheduler.
            alarm.address = session.message.address;
            alarms[alarm.title] = alarm;
            
            // Send confirmation to user
            var date = new Date(alarm.timestamp);
            var isAM = date.getHours() < 12;
            session.send('Creating alarm named "%s" for %d/%d/%d %d:%02d%s',
                alarm.title,
                date.getMonth() + 1, date.getDate(), date.getFullYear(),
                isAM ? date.getHours() : date.getHours() - 12, date.getMinutes(), isAM ? 'am' : 'pm');
        } else {
            session.send('Ok... no problem.');
        }
    }
]);

dialog.matches('builtin.intent.alarm.delete_alarm', [
    function (session, args, next) {
        // Resolve entities passed from LUIS.
        var title;
        var entity = builder.EntityRecognizer.findEntity(args.entities, 'builtin.alarm.title');
        if(!entity){
           next();
        }
        if (entity) {
            // Verify its in our set of alarms.
            title = builder.EntityRecognizer.findBestMatch(alarms, entity.entity);
        }
        // Prompt for alarm name
        if (!title) {
            builder.Prompts.choice(session, prompts.alarmdelete, alarms);
        } else {
            next({ response: title });
        }
    }, function (session, results) {
        // If response is null the user canceled the task
        if (results.response) {
            delete alarms[results.response.entity];
            session.send("Deleted the '%s' alarm.", results.response.entity);
        } else {
            session.send('Ok... no problem.');
        }
    }
]);

// Very simple alarm scheduler
var alarms = {};
    setInterval(function () {
        var now = new Date().getTime();
        for (var key in alarms) {
            var alarm = alarms[key];
            if (now >= alarm.timestamp) {
                var msg = new builder.Message()
                    .address(alarm.address)
                    .text("Here's your '%s' alarm.", alarm.title);
                bot.send(msg);
                delete alarms[key];
            }
        }
    }, 15000);

 //Unkown Command
dialog.onDefault(builder.DialogAction.send(prompts.generalerror));
