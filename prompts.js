module.exports = {
    botHello: "Hello I am Spark, how can I help you today? If you need some help type 'help'",
    botHelp: "Here is a list of my available commands: 'setalarm', 'weather', 'news' ",
    msgHi: "Heya! :) Type 'help' if you can't remember what I can do.",
    promptCategory: "Which category would you like?",
    choiceCategories: "Technology|Science|Sports|Business|Entertainment|Politics|Health|World|(quit)",
    locationcheck: "What location?",
    alarmname: "What would you like to call your alarm?",
    alarmtime: "What time would you like to set the alarm for?",
    alarmdelete: "Which alarm would you like to delete?",
    loading: "Give me a moment :)",
    newserror: "I'm sorry, that is not a recognised category.",
    generalerror:"I'm sorry I didn't understand, please try again",
}