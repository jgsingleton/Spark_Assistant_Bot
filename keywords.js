//==================================================================================
// 'Spark' - Intelligent Software Agent Dissertation Project by Jesse Singleton
//==================================================================================
// keywords.js -- Simple Regex Integration
//==================================================================================
// Adapted from Microsoft (Alyssa Ong ) https://github.com/alyssaong1/NodeNewsBot.
//================================================================================== 

function matchExact(words){
    var regStr = "\\b(";
    for (var i=0; i < words.length; i++) {
        //first word in array
        if (i === 0){
            regStr += words[i];
        } else {
            regStr += "|"+words[i];
        }
    }
    regStr += ")\\b";
    var regExp = new RegExp(regStr, "i");
    console.log(regExp);
    return regExp;
}

// This regex checks that the word is a substring of what the user said
function matchWithin(words){
    var regStr = "\\W*(";
    for (var i=0; i < words.length; i++) {
        //first word in array
        if (i === 0){
            regStr += words[i];
        } else {
            regStr += "|"+words[i];
        }
    }
    regStr += ")\\W*";
    var regExp = new RegExp(regStr, "i");
    console.log(regExp);
    return regExp;
}

module.exports = {
    hello: matchWithin(["hello","hi","hey","heya"]),
    news: matchWithin(["news"]),
    help: matchWithin(["help","help me","commands"]),
    searchnews: matchWithin(["find", "search", "keyword"]),
    menu: matchExact(["menu"]),
    trending: matchWithin(["trend"]),
    topnews: matchExact(["top","category","categories"]),
    start: matchExact(["start", "get started"]),
    weather:matchExact(["weather", "what's the weather today", "weather today"]),
    alarm:matchExact(["set alarm", "alarm"])
}